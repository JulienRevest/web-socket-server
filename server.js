const express = require('express');
const app = express();
const server = require('http').Server(app);
const pty = require('./node_modules/node-pty');
const WebSocket = require('./node_modules/ws');
const os = require('os');

app.use('/', express.static('.'));
const wss = new WebSocket.Server({ server });

var shell = os.platform() === 'win32' ? 'powershell.exe' : 'bash';
let ptyProcess;

function killProcess() {
	console.log('Client disconnected, killing shell')
	ptyProcess.kill();
}

/** Handling connection event */
wss.on('connection', ws => {
	console.log('Client connected, spawning shell');
	ptyProcess = pty.spawn(shell, [], {
    	name: 'xterm-color',
		// cols: 80,
		// rows: 30,
		cwd: process.env.HOME,
		env: process.env,
		handleFlowControl: true
	});
	ptyProcess.onData(function(data) {
		ws.send(data);
	});
	ws.on('message', message => {
		//m = JSON.parse(message);	// TODO: Possibly send JSON messages to add more properties 

		// Directly write in our shell
		if (message) {
			ptyProcess.write(message);
		}
		//  else if (message.resize) {
		// 	ptyProcess.resize(message.resize[0], message.resize[1]);
		// }

	});
	// Kill our shell on socket close
	ws.on('close', killProcess);
});

server.listen(process.env.PORT || 3000, () => {
	console.log(`Server started on port ${server.address().port}`);
});
