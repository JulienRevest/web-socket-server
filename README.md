# Bud E-learning WebSocket server

Simple WebSocket server that exposes our container shell
By default, it exposes a socket on the port `3000`, but it can be changed by setting the `PORT` environnement variable

### Running the server
```
yarn install
export PORT=5000
node server.js
```

### Packaging
***If you generated the node_modules folder on windows, packaging will succeed, but the server will crash when ran. Avoid doing so and use Linux to generate this folder.***

To pack this application in a single file, for conveniance, you can use pkg:
```
yarn global add pkg
yarn pkg
```

You can then run the server like any other executable:
```
./websocket-server
```